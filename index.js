const express = require("express");
const mysql = require("mysql2");

const app = express();
app.use(express.json())

const connection = mysql.createConnection({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "1234",
    database: "management"
});

connection.connect((error) => {
    if (error) {
        console.log("Cannot connect to database");
        throw error;
    }
    console.log("Connect to database");
})


app.get("/food", (req, res) => {
    const query = `SELECT * FROM food`
    connection.execute(query, (error, result) => {
        if (error) {
            res.status(500).json({ error: error })
            return
        }
        res.status(200).json({ data: result })
    })
})

app.listen(4000)